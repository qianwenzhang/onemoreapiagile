import chai from 'chai'
const expect = chai.expect
// const _ = require("lodash")
const dotenv = require("dotenv")
const request = require("supertest")
dotenv.config()

let server = require("../../../bin/www")
describe("Records", ()=> {
  describe("POST /addRecord", () => {
    describe("a new record is inserted into database", () => {
      it("should return confirmation and update database ", () => {
        const record = {
          suppFocusTime: "25"
        }
        return request(server)
          .post("/addRecord")
          .send(record)
          .expect(200)
          .then(res => {
            expect(res.body.message).equal("Record Successfully Added!" )
          //   expect({message: "User has registered successfully"})
          })
      })
    })
  })

})
