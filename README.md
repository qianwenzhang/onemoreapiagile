# Assignment 2 - Agile Software Practice.

Name: Qianwen Zhang

## Client UI.
#### See more functions, check video link: https://youtu.be/8twO5IdHNkE

![][Home]

>>Users should login first, otherwise they can do nothing on the website(redirect)

![][Login]

>>3 ways are provide for users, including facebook, google account, and normal registration.

![][SignInWithFaceBook]

>>The user name and user profile will be shown on the page after logging in.

![][SetTimer]

>>Choose plant, minutes and write some description to start timer.

![][TimerGoes]

>>A timer will be shown on the page.


![][QuitFocusing]

>>Click Stop button, there will be an alert, you can choose whether to go on or not.


![][IfYouQuit]

>>It will show you how many coins you got.


![][CheckRecord]

>>No matter you interrupt your concentration or not, you can check your planting record.


![][CRUD Tag]

>>CRUD Tag.


![][FuzzySearchFriend]

>>Use fuzzy search to get other users.(Search user name, eg: friend name: Qianwen, you can input Qia)

![][Logout]

>> Logout.




[Home]: ./img/Home.png
[Login]: ./img/Login.png
[SignInWithFaceBook]: ./img/SignInWithFB.png
[SetTimer]: ./img/SetTimerForFocusing.png
[TimerGoes]: ./img/TimerGoes.png
[QuitFocusing]: ./img/QuitFocusing.png
[IfYouQuit]: ./img/ShowCoins.png
[CheckRecord]: ./img/CheckPlantingRecord.png
[CRUD Tag]: ./img/CRUDTag.png
[FuzzySearchFriend]: ./img/FuzzySearchFriend.png
[Logout]: ./img/Logout.png


Links:
gitlab-vue: https://gitlab.com/qianwenzhang/vueprojecttm/pipelines
gitlab-api: https://gitlab.com/qianwenzhang/web-api-projecttimemanagement

Heroku: https://taskmanagementqwzhang.herokuapp.com/

[![pipeline status](https://gitlab.com/qianwenzhang/onemoreapiagile/badges/master/pipeline.svg)](https://gitlab.com/qianwenzhang/onemoreapiagile/commits/master)

[![coverage report](https://gitlab.com/qianwenzhang/onemoreapiagile/badges/master/coverage.svg)](https://gitlab.com/qianwenzhang/onemoreapiagile/badges/master/coverage.svg?job=coverage)

