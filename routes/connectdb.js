// const dotenv = require("dotenv")
// dotenv.config()

// const mongoose =require("mongoose")
// // let mongodbUri = "mongodb+srv://qianwenzhangnancy:zqw123456@wit-qianwenzhang-cluster-yyg37.mongodb.net/taskmanagementdb"
// mongoose.connect(process.env.MONGO_URI)
//
// let db = mongoose.connection
// db.on("error",function (err) {
//   console.log("Unable to Connect to  [" + db.name + "]",err)
// })
// db.once("open",function () {
//   console.log("Successfully Connected to  [" + db.name + "]")
// })
//
// module.exports = mongoose

let mongoose = require("mongoose")
const dotenv = require("dotenv")
dotenv.config()

mongoose.connect(process.env.MONGO_URI, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})

let db = mongoose.connection

db.on("error", function (err) {
  console.log("Unable to Connect to [ " + db.name + " ]", err)
})

db.once("open", function () {
  console.log("Successfully Connected to [ " + db.name + " ]")
})

module.exports = mongoose

